<div id="map">

</div>

<footer id="information">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4">
          <h3>Impresszum</h3>
          <span><strong>Felelős kiadó:</strong>A Web-rendszer technológia hátterét az IntelliMed Hungária Kft. biztosítja <a target="_blank"href="http://www.intellimed.eu">[ www.intellimed.eu ]</a></span>

          <span><strong>Főszerkesztő:</strong> Dr. Péter Antal, az MTT főtitkára</span>
          <span>Technikai ügyekben kérjük, írjon a: <a href="mailto:support@transzplant.hu">support@transzplant.hu</a> e-mail címre.</span>

        </div>
        <div class="col-md-4">
          <h3>Elérhetőségeink</h3>
          <span><strong>Magyar Transzplantációs Társaság</strong> </span>
          <span>
            SE ÁOK Transzplantációs és Sebészeti Klinika 1082 Budapest, Baross utca 23-25
          </span>
        </div>
        <div class="col-md-4">
          <h3>Üzemeltető</h3>
          <a target="_blank" href="http://www.intellimed.eu/"><img class="logo" src="/components/img/intellimed-logo.svg" alt=""></a>
        </div>
      </div>
    </div>
  </div>
</footer>
