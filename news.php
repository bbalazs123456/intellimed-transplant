<?php include 'head.php' ?>
<?php include 'header.php' ?>

<div class="content news">
    <div class="container" id="content-news-page">
        <div class="page-title">
            A MHTT legfrissebb hírei
        </div>

        <h1 class="title">
            Felhívás jelölésre az MHTT 2019. évi társasági nagydíjaira
        </h1>
        <div class="publication-data">
            <span class="category news-category-32">Általános</span>
            <span class="showdate">2019.01.15.</span>
            <span class="author"><label>Szerző:</label>MTT</span>
            <span class="source"><label>Forrás:</label></span>
        </div>
        <hr>
        <div class="image">
            <img src="/components/img/inprogress.jpg" alt="">
        </div>
        <div class="lead">

        </div>
        <div class="text">
          <p>A Magyar Transzplantációs Társaság tagdíja a Társaság több mint 20 éves megalakulása óta 2000,- Ft volt évente. Elsősorban a szponzorálási lehetőségeket érintő különféle szabályozások, megszorítások miatt a Társaság bevételei jelentősen csökkentek, ezért elkerülhetetlenné vált a tagdíj emelése. A tagdíj emelésnél a vezető cél az volt, hogy a tagdíj bevétel legalább a Társaság működési költségeit fedezze. Az MTT Vezetősége alapos mérlegelés után differenciált tagdíj emelést javasolt, melyet a Társaság 2018. május 7-i (rendkívüli) Közgyűlésén elfogadott.
          Ezek szerint a Magyar Transzplantációs Társaság éves tagdíja <strong>2019. január 1-től a következőképpen alakul:</strong>
          <ol>
            <li>Szakorvos 35 év felett: 8.000,- Ft</li>
            <li>Szakorvos 35 év alatt: 4.000,- Ft</li>
            <li>Egyéb diplomás, szakdolgozó, PhD hallgató, rezidens, szakorvos jelölt, GYES/GYED-en levő, nyugdíjas: 2.000,- Ft</li>
          </ol>

          Köszönjük megértését!

          A Magyar Transzplantációs Társaság Vezetősége </p>
        </div>

        <div class="history-back">
            <a href="javascript: history.back()">
                <img src="/components/img/back-arrows.png" alt="">
                <span>Vissza</span>
            </a>
        </div>
    </div>

</div>

<?php include 'footer.php' ?>

<script src="components/js/style.js"></script>
