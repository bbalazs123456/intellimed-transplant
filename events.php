<?php include 'head.php' ?>
<?php include 'header.php' ?>

<section class="content inprogress">
  <span>A tartalom felt�l�se folyamatban..</span>
</section>

<?php include 'partners.php' ?>
<?php include 'footer.php' ?>

<script>

    //scroll-to

    $(".scroll-to").click(function (e) {
      e.preventDefault;
      var target = $(this).attr('href');
      $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000);
    });

	// ===== Scroll to Top ====
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
				$('#return-to-top').fadeIn(200);    // Fade in the arrow
			} else {
				$('#return-to-top').fadeOut(200);   // Else fade out the arrow
			}
		});
		$('#return-to-top').click(function() {      // When arrow is clicked
			$('body,html').animate({
				scrollTop : 0                       // Scroll to top of body
			}, 500);
		});
</script>
