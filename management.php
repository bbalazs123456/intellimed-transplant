<?php include 'head.php' ?>
<?php include 'header.php' ?>

<section class="content bg-block animated fadeIn">
  <div class="container">

    <div class="management-block">

        <div class="row">

          <div class="col-md-9">

            <h1>A Társaság vezető szervei, bizottságai</h1>

            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>

            <hr style="margin-bottom: 80px;">

            <div style="animation-delay: 1.5s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="components/img/person1.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Person 1</h3>
                <span>Position</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <div style="animation-delay: 2s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="components/img/person2.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Person 2</h3>
                <span>Position</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <div style="animation-delay: 2.5s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="components/img/person4.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Person 3</h3>
                <span>Position</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <div style="animation-delay: 3s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="components/img/person3.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Person 4</h3>
                <span>Position</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <section class="billing">
              <h1>Tisztelt MTT-Tagtársunk!</h1>
              <hr>

                        <span>A Magyar Transzplantációs Társaság tagdíja a Társaság több mint 20 éves megalakulása óta 2000,- Ft volt évente. Elsősorban a szponzorálási lehetőségeket érintő különféle szabályozások, megszorítások miatt a Társaság bevételei jelentősen csökkentek, ezért elkerülhetetlenné vált a tagdíj emelése. A tagdíj emelésnél a vezető cél az volt, hogy a tagdíj bevétel legalább a Társaság működési költségeit fedezze. Az MTT Vezetősége alapos mérlegelés után differenciált tagdíj emelést javasolt, melyet a Társaság 2018. május 7-i (rendkívüli) Közgyűlésén elfogadott.
            Ezek szerint a Magyar Transzplantációs Társaság éves tagdíja <strong>2019. január 1-től a következőképpen alakul:</strong>

            <ol>
              <li>Szakorvos 35 év felett: 8.000,- Ft</li>
              <li>Szakorvos 35 év alatt: 4.000,- Ft</li>
              <li>Egyéb diplomás, szakdolgozó, PhD hallgató, rezidens, szakorvos jelölt, GYES/GYED-en levő, nyugdíjas: 2.000,- Ft</li>
            </ol>

            Köszönjük megértését!

            A Magyar Transzplantációs Társaság Vezetősége

            A tagdíjat átutalással lehet befizetni a Magyar Transzplantációs Társaság 11708001-20519184-es számlaszámára!

            A tagdíjbefizetés állapotáról Maléth Anikónál lehet érdeklődni a 06 20 825 8574-es telefonszámon.</span>
            </section>

          </div>

          <div style="animation-delay: 4s;" class="col-md-3 animation animated fadeInUp">
            <div class="calendar">
              <h3>Eseménynaptár</h3>
            </div>

			<div class="img-section">
				<img src="components/img/man.jpg" alt="">
			</div>

			<div class="credit-account-number">
				<h3>Bankszámla szám</h3>

				<hr>
        <img src="components/img/szamlaszam.jpg" alt="OTP BANK: 11708001-2051918">
        <br>
        <span><strong>OTP BANK: 11708001-2051918</strong></span>

			</div>

          </div>

        </div>

    </div>

  </div>
</section>

<?php include 'footer.php' ?>

<script>

    //scroll-to

    $(".scroll-to").click(function (e) {
      e.preventDefault;
      var target = $(this).attr('href');
      $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000);
    });

	// ===== Scroll to Top ====
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
				$('#return-to-top').fadeIn(200);    // Fade in the arrow
			} else {
				$('#return-to-top').fadeOut(200);   // Else fade out the arrow
			}
		});
		$('#return-to-top').click(function() {      // When arrow is clicked
			$('body,html').animate({
				scrollTop : 0                       // Scroll to top of body
			}, 500);
		})
</script>

<script src="components/js/style.js"></script>
