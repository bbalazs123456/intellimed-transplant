<?php include 'head.php' ?>
<?php include 'header.php' ?>

<section class="bg-block2 animated fadeIn">
	<div class="container">
		<div class="document-block">

			<div class="row management-block">

					<h1>Dokumentumok</h1>
					<span>Tartalomszolgáltatásunkban részt vevő partnereink dokumentumai:
					Fontos: A dokumentumokban szereplő információk nem a Magyar Hematológiai és Transzfuziológiai Társaság hivatalos álláspontját jelentik!</span>

					<div id="roche" class="col-md-12 document-block">
						<div class="row">

							<h3 style="animation-delay: 0.5s;" class="animated fadeIn" >Roche (Magyarország)</h3>
							<hr>


							<div class="col-md-4 container-blocks animated fadeInUp">

								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div class="col-md-8 animated fadeInUp">

								<img class="assistants" src="<?= $base_url ?>components/img/woman2.jpg">

							</div>

						</div>
					</div>

					<div class="col-md-12 document-block bg-color">
						<div class="row">
							<h3 style="animation-delay: 0.5s;" class="animated fadeIn" >Általános</h3>
							<hr>

							<div style="animation-delay: 2s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/word_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>


							<div style="animation-delay: 2.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>


							<div style="animation-delay: 3s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/word_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>
						</div>
					</div>

					<div style="animation-delay: 2s;" class="col-md-12 document-block">

						<div class="row">
							<h3>Szakmai</h3>
							<hr>

							<div style="animation-delay: 3.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>Mintavételi tájékoztató a nagy érzékenységű, mennyiségi BCR-ABL1 mRNS kimutatáshoz</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>
						</div>
					</div>

					<div class="col-md-12 document-block bg-color ">
						<div class="row">
							<h3>Páciens</h3>
							<hr>

						<div style="animation-delay: 4s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>Mintavételi tájékoztató a nagy érzékenységű, mennyiségi BCR-ABL1 mRNS kimutatáshoz</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>
						</div>

						<div style="animation-delay: 4.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>Mintavételi tájékoztató a nagy érzékenységű, mennyiségi BCR-ABL1 mRNS kimutatáshoz</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

						</div>


						</div>
					</div>

					<div style="animation-delay: 3s;" class="col-md-12 document-block ">
						<div class="row">
							<h3>MHTT e-Hírlevél</h3>
							<hr>

							<div style="animation-delay: 5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div style="animation-delay: 5.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

						<div style="animation-delay: 6s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

						<div style="animation-delay: 6.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div style="animation-delay: 7s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div style="animation-delay: 7.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div style="animation-delay: 8s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

						</div>
					</div>

					<div class="col-md-12 document-block bg-color">
						<div class="row">
							<h3>Társasági dokumentumok</h3>
							<hr>

							<div row>
							<div style="animation-delay: 8.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>
					</div>
							</div>

							<div style="animation-delay: 9s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div style="animation-delay: 9.5s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>

							<div style="animation-delay: 10s;" class="col-md-4 container-blocks animated fadeInUp">
								<a target="_blank" href="http://www.mhtt.hu/upload/haematologia/partner/a_roche-rol_diohejban.pdf?web_id="><img src="<?= $base_url ?>components/img/document.png">
								<img src="<?= $base_url ?>components/img/pdf_ikon.gif">
								<span>A Roche-ról dióhéjban</span></a>
								<p><b>Publikálva:</b> 2018.12.14.</p>
								<p><b>Méret:</b> 0,421 Mb</p>

							</div>
						</div>
					</div>


			</div>

			<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>

		</div>
	</div>
</section>

<?php include 'partners.php' ?>
<?php include 'footer.php' ?>

<script>

    //scroll-to

    $(".scroll-to").click(function (e) {
      e.preventDefault;
      var target = $(this).attr('href');
      $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000);
    });

	// ===== Scroll to Top ====
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
				$('#return-to-top').fadeIn(200);    // Fade in the arrow
			} else {
				$('#return-to-top').fadeOut(200);   // Else fade out the arrow
			}
		});
		$('#return-to-top').click(function() {      // When arrow is clicked
			$('body,html').animate({
				scrollTop : 0                       // Scroll to top of body
			}, 500);
		});
</script>

<script src="<?= $base_url ?>components/js/style.js"></script>
