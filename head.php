<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Magyar Transzplantációs Társaság</title>
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="components/css/style.css">
<link rel="stylesheet" type="text/css" href="components/css/responsive.css">
<link rel="stylesheet" type="text/css" href="components/css/animate.css-master/animate.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="shortcut icon" href="/components/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="/components/img/favicon.ico" type="image/x-icon">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>

// google map api
function initMap() {

    var location = {lat: 47.489059, lng: 19.06666};
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 15, center: location});
    var marker = new google.maps.Marker({position: location, map: map});
}
</script>

<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTX28ZdGmaxBRC9b1Lk3zfYhCH2_mJceA&callback=initMap">
</script>

<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
<script src="components/js/style.js"></script>
