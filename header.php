<header id="navbar" class="header">
  <div class="container">

    <nav class="navbar navbar-default navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#">
          <a href="/"><img class="logo" src="/components/img/tarsasag_logo2.png" alt=""></a>
          </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="scroll-to" href="#about-us">Rólunk</a></li>
            <li><a class="scroll-to" href="#news">Hírek</a></li>
            <li><a class="scroll-to" href="#membership">Tagság</a></li>
            <li><a class="scroll-to" href="management.php">Vezetőség</a></li>
            <li><a class="scroll-to" href="inprogress.php">Doumentumok</a></li>
			      <li><a class="scroll-to" href="inprogress.php">Páciensek</a></li>
			      <li><a class="scroll-to" href="inprogress.php">Rendezvények</a></li>
            <li><a class="scroll-to" href="#information">Információ</a></li>
            <li><a id="modalBtn" class="add-btn login">Bejelentkezés</a></li>
          </ul>
        </div>

        <div id="simpleModal" class="modal">
          <div id="popup" class="modal-content">

            <div class="col-md-12">
              <div class="row">

                <div class="modal-head">
                  <span>Kérjük adja meg a szükséges adatokat!</span>
                  <span id="closeBtn" class="closeBtn">&times;</span>
                </div>

				<div class="registration-block">
				<span>Amennyiben nem rendelkezik Felhasználó fiókkal kérjük kattintson a  gombra!</span>

				<a href=".<?= $base_url ?>subpage/inprogress.php"><p>regisztrálok</p></a>

				</div>

                <div class="modal-article">

                      <div class="user-input">
                        <input type="text" name="" value="" placeholder="Felhasználónév">
                      </div>

                      <div class="user-password">
                        <input type="password" name="" value="" placeholder="Jelszó">
                      </div>

                      <div class="row">

                        <div class="col-md-6 button-section">
                          <button placeholder="Felhasználónév" id="registeredSucces" class="add-btn btn-login" type="button">Belépés</button>
                        </div>

                        <div class="col-md-6 button-section">
                          <button placeholder="Jeszó" id="registeredCancel" class="add-btn btn-cancel" type="button">Mégse</button>
                        </div>

                      </div>

                </div>
              </div>
            </div>

          </div>
        </div>

    </nav>
  </div>
</header>
