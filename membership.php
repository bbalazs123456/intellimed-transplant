<section id="membership" class="membership">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <h1>Tisztelt MTT-Tagtársunk!</h1>
        <hr>

        <span>A Magyar Transzplantációs Társaság tagdíja a Társaság több mint 20 éves megalakulása óta 2000,- Ft volt évente. Elsősorban a szponzorálási lehetőségeket érintő különféle szabályozások, megszorítások miatt a Társaság bevételei jelentősen csökkentek, ezért elkerülhetetlenné vált a tagdíj emelése. A tagdíj emelésnél a vezető cél az volt, hogy a tagdíj bevétel legalább a Társaság működési költségeit fedezze. Az MTT Vezetősége alapos mérlegelés után differenciált tagdíj emelést javasolt, melyet a Társaság 2018. május 7-i (rendkívüli) Közgyűlésén elfogadott.
        Ezek szerint a Magyar Transzplantációs Társaság éves tagdíja 2019. január 1-től a következőképpen alakul:</span>

        <ol>
          <li>Szakorvos 35 év felett: 8.000,- Ft</li>
          <li>Szakorvos 35 év alatt: 4.000,- Ft</li>
          <li>Egyéb diplomás, szakdolgozó, PhD hallgató, rezidens, szakorvos jelölt, GYES/GYED-en levő, nyugdíjas: 2.000,- Ft</li>
        </ol>

        <span>Köszönjük megértését!

        A Magyar Transzplantációs Társaság Vezetősége

        A tagdíjat átutalással lehet befizetni a Magyar Transzplantációs Társaság 11708001-20519184-es számlaszámára!

        A tagdíjbefizetés állapotáról Maléth Anikónál lehet érdeklődni a 06 20 825 8574-es telefonszámon.


        Felhívjuk a Társaság soraiba újonnan jelentkezők figyelmét, hogy a tagfelvételi kérelem elküldése nem jár automatikus tagsági viszonnyal!

        A beérkező kérelmekről a Társaság vezetősége dönt, majd értesíti a jelentkezőt a felvételről, vagy a tagfelvételi kérelem elutasításáról. Az elbírálás hosszabb időt is igénybe vehet, ezzel kapcsolatban szíves megértését és türelmét kérjük.</span>

      </div>
    </div>
  </div>
</section>
