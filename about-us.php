<section id="about-us" class="about-us pattern">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <h1>Rólunk</h1>
          <hr>

          <span><strong>A társaság célja:</strong><br><br> A hazai transzplantológia fejlesztése, a tagok tudományos ismereteinek bővítése, valamint további célja az, hogy őrködjön tagjai erkölcsi tekintélye felett és érvényesítse erkölcsi és anyagi érdekeit a közérdekkel összhangban, ismertesse a tudományág hazai és külföldi eredményeit, elősegítse a tudományos eredmények gyakorlati felhasználását, a magyar kutatási eredmények minél szélesebb körű megismertetését bel- és külföldön egyaránt.</span>

        </div>
        <div class="col-md-6">
          <img class="animated pulse" src="components/img/transplant.png" alt="">
        </div>
      </div>
    </div>
  </div>
</section>






<section class="doing">
  <div class="container">
    <div class="col-md-12">

      <h1>A társaság tevékenysége:</h1>
      <hr>

      <ol>
        <li>tagjai és vendégei számára rendszeresen előadásokat, referáló és vitaüléseket, ankétokat, szimpóziumokat, vándorgyűléseket, nagygyűléseket, nemzetközi konferenciákat és kongresszusokat rendezhet,</li>
        <li>szakfolyóiratokat adhat ki, javaslataival és vírálataival segítheti az orvostudományi könyvkiadást,</li>
        <li>figyelemmel kíséri a szakterületét érintő oktatási kérdéseket, javaslatokkal és bírálatokkal segíti azok megoldását, támogatja a magas szintű szakképzést és továbbképzést,</li>
        <li>javaslatokat, ajánlásokat dolgoz ki vizsgáló módszerek, terápiás elvek vonatkozásában,</li>
        <li>feladatai megoldása érdekében pályázatokat hírdethet, jutalmakat tűzhet ki, emlékérmeket és díjakat alapíthat,</li>
        <li>szükség szerint bevonja munkájába mindazokat a szakembereket, akik felkészültségüknél fogva a Társaság célkitűzéseit elő tudják mozdítani,</li>
        <li>Tudományos kiállításokat rendezhet</li>
        <li>általában előmozdítja tagjainak szakmai érvényesülését, munkafeltételeinek javítását, kapcsolatot létesít és együttműködik más nemzeti és nemzetközi szervezetekkel, Társaságokkal, szakmai kollégiumokkal a hatályos rendelkezések betartásával csatlakozhat hazai és nemzetközi tudományos szervezetek szövetségéhez, ezeknek tagsági díjat fizethet, azonos területen működő külföldi Társaságokkal két-, vagy többoldalú együttműködési megállapodásokat köthet, továbbá segíti, irányíja, összehangolja és ellenőrzi tagjai ezzel kapcsolatos tevékenységét,</li>
        <li>anyagi lehetőségeinek határain belül támogatja tagjai külföldi kongresszusok, szakmai rendezvényeken való rész vételét</li>
      </ol>
    </div>
  </div>
</section>
